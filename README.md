client—warring
=============

##服务端引擎:
http://www.kbengine.org

##GO!

	##创建AssetBundles

	unity3d(菜单上)->Publish->Build Pulish AssetBundles - autoAll

	期间可能一直出现"Moving file failed"， 需要点击"try again"让其继续, 
	这可能是unity3d在移动文件时文件被占用造成的。
	执行完毕后检查 Assets->StreamingAssets是否有内容。


##编译:

	unity3d File->Build Settings->Scenes In Build选择scenes/go.unity->Platform

	选择Web Player->Build。


## web服务器部署文件夹结构

	->(服务器根目录)
		- StreamingAssets (创建AssetBundles生成的文件夹)
		- ui (Assets下的ui文件夹可以直接拷贝过来)
		- crossdomain.xml
		- initLogo.png
		- initProgressBar.PNG
		- initProgressFrame.PNG
		- index.html
		- Assets.unity3d (在unity3d编译时生成的文件)
		- Assets.html (在unity3d编译时生成的文件)


##运行:

	1. 启动kbengine服务端

	2. 浏览器访问localhost

	如不清楚请下载发布版demo， 并按照其中的文件夹结构放置并看压缩包内文档教程如何启动:

	https://sourceforge.net/projects/kbengine/files/


##日志:

	Windows XP: C:\Documents and Settings\username\Local  Settings\Temp\UnityWebPlayer\log

	Windows Vista/7: C:\Users\username\AppData\Local\Temp\UnityWebPlayer\log
